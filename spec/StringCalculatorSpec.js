describe('StringCalculator', () => {
  
  
  it("Retun 0 if string is empty", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("")

    expect(operation).toEqual(0)
  })

  it("Retun a number if string has one number", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("1")

    expect(operation).toEqual(1)
  })

  it("Retun the sum of two numbers", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("1,4")

    expect(operation).toEqual(5)
  })

  it("Retun the sum of many numbers", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("1,4,6,4,3")

    expect(operation).toEqual(18)
  })

  it("Retun the sum if are separated for \n too", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("1\n2,3")

    expect(operation).toEqual(6)
  })

  it("Retun the sum if are separated for any character that are not a number", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("//;\n1;2")
    let anOperation = stringCalculator.add("4/?+5/;\n1;2")

    expect(operation).toEqual(3)
    expect(anOperation).toEqual(12)
  })  

  it("With a negative number will throw an exception 'negatives not allowed' - and the negative that was passed", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("1,4,-1")
    let anOperation = stringCalculator.add("1,4,-1,-6")
    
    expect(operation).toEqual("negatives not allowed: -1")
    expect(anOperation).toEqual("negatives not allowed: -1,-6")
  })  

  it("If there is a big number ignore it", () =>{
    let stringCalculator = new StringCalculator()

    let operation = stringCalculator.add("2, 1001")

    expect(operation).toEqual(2)
  })

})
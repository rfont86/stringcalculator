const SEPARATORS = /[^0-9\-]/
const BIG_NUMBER = 1000

class StringCalculator{

  add(listOfStringNumbers){
    let result;

    let listDivided = this._divideList(listOfStringNumbers)
    let listOfNumbers = this._toNumbers(listDivided)
    let listToSum = this._ignoreBigNumbers(listOfNumbers)

    if(!this._withoutNegatives(listToSum)){ result = this._sumItems(listToSum) }
    if(this._withoutNegatives(listToSum)){ result = this._trowErrorMessage(listToSum) }

    return result
  }

  _ignoreBigNumbers(list){
    const result = list.filter( number => number < BIG_NUMBER)
    return result
  }

  _trowErrorMessage(list){
    const negativeListNumbers = list.filter( number => number < 0)  //DUDA-> 0 es un magic number?
    return 'negatives not allowed: ' + negativeListNumbers
  }

  _withoutNegatives(list){
    const result = list.some( number => number < 0 )
    return result
  }

  _sumItems(list){
    const result = list.reduce((accumulator, currentValue) => {return accumulator + currentValue})
    return result 
  }

  _divideList(aString){
    const result = aString.split(SEPARATORS)
    return result
  }
  
  _toNumbers(aString){
    const result = aString.map( number =>{return Number(number)})
    return result
  }

}

